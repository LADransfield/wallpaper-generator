# Wallpaper generator

Python script that generates an unique desktop wallpaper every 15 minutes.

Further information can be found in the [wiki](https://gitlab.com/LADransfield/wallpaper-generator/wikis/home).