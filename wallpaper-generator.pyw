#!/usr/bin/env python

import os
import sys
import platform
import math
import random
import threading
import time
import svgwrite
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM

# classes
class System:

    def __init__(self):

        # get the name of the OS
        self.platform = platform.system()

        if self.platform == 'Windows':
            # set directory and get height and width from Windows
            self.directory = r'C:\temp\wallpaperGenerator'

            import win32api
            monitors = win32api.EnumDisplayMonitors()
            width = 0
            height = 0
            for monitor in monitors:
                left, bottom, right, top = monitor[2]
                width = abs(min(width, left)) + max(width, right)
                height = abs(min(height, bottom)) + max(height, top)
            self.width = width
            self.height = height

        # elif self.platform == 'Linux':
            # import gtk
            # # set directory and get height and width from GTK
            # self.directory = r'\tmp\wallpaperGenerator'
            # self.width = gtk.gdk.screen_width()
            # self.height = gtk.gdk.screen_height()
        
        if not os.path.isdir(self.directory):
            # if the directory doesn't exist, create it
            os.makedirs(self.directory)

        # create image location
        self.svgLoc = os.path.join(self.directory, 'image.svg')
        self.pngLoc = os.path.join(self.directory, 'image.png')

    def set_wallpaper(self):

        # check the image file exists
        if os.path.isfile(self.pngLoc):
            print(self.pngLoc)
            if self.platform == 'Windows':
                print('Windows')
                import ctypes
                set_wallpaper = 20
                set_update = 0
                # set wallpaper for Windows
                if (float(sys.version_info[0]) + float(sys.version_info[1] / 10)) <= 3.50:
                    ctypes.windll.user32.SystemParametersInfoA(\
                        set_wallpaper, 0, self.pngLoc, set_update)
                else:
                    ctypes.windll.user32.SystemParametersInfoW(\
                        set_wallpaper, 0, self.pngLoc, set_update)

            # elif self.platform == 'Linux':
            #     print('Linux')
            #     import gtk

class Image:

    def __init__(self, dir, size):
        self.directory = dir
        self.size = size
        # create image location
        self.svgLoc = os.path.join(self.directory, 'image.svg')
        self.pngLoc = os.path.join(self.directory, 'image.png')
        # initialise drawing
        self.dwg = svgwrite.Drawing(self.svgLoc, size=(self.size[0], self.size[1]))
        # generate the pattern
        Pattern(self.dwg, self.size)

    def save(self):

        if os.path.isfile(self.pngLoc):
            os.remove(self.pngLoc)

        # save svg
        self.dwg.save()

        # save png
        drawing = svg2rlg(self.svgLoc)
        renderPM.drawToFile(drawing, self.pngLoc, fmt='PNG')

class Pattern:

    def __init__(self, drawing, size):
        self.background = [0, 161, 176]
        self.primary = [0, 126, 138]
        self.accent1 = [252, 177, 25]
        self.accent2 = [252, 53, 25]
        self.width = size[0]
        self.height = size[1]
        self.dwg = drawing
        self.__set_background__()

        randType = random.random()

        if randType <= 0.33:
            print('Stripes')
            self.__stripes__()
        elif randType <=0.66:
            print('Spots')
            self.__spots__()
        else:
            print('Tiles')
            self.__tiles__()

    def __set_background__(self):

        background = self.dwg.add(\
        	self.dwg.rect(insert=(0, 0), size=(self.width, self.height)))
        background.fill(\
        	svgwrite.rgb(self.background[0], self.background[1], self.background[2]), opacity=1)
        return self.dwg


    def __select_colour__(self, primary, accent1, accent2):

        randCol = random.random()

        if randCol <= 0.60:
            return primary
        elif randCol <= 0.80:
            return accent1
        elif randCol <= 1.00:
            return accent2


    def __spots__(self):

        # define orbital spacing
        orbits = 60 # number of orbits
        obitDistance = 65 # distance between each orbit

        # define angular spacing
        angleScaler = 3

        # define seed location
        seedWidth = float(self.width) * 0.6 # can only appear inside centre 2/3
        seedHeight = float(self.height) * 0.6
        minWidth = float(self.width) * 0.2
        minHeight = float(self.height) * 0.2
        seedX = minWidth + (seedWidth * random.random())
        seedY = minHeight + (seedHeight * random.random())

        # select imploding or exploding
        direction = random.random()

        if direction >= 0.5:
            # starts with large seed radius
            seedRadius = random.uniform(50, 55)
            print('Explode')
        else:
            # starts with small seed radius
            seedRadius = random.uniform(2, 7)
            print('Implode')

        # loop through orbital rings
        for orbit in range(1, orbits):

            for dot in range(0, orbit * angleScaler):

                if direction >= 0.5:
                    # gets smaller outwards
                    seedRadius = \
                    seedRadius * random.uniform(0.998, 0.999)
                else:
                    # gets larger outwards
                    seedRadius = \
                    seedRadius * random.uniform(1.001, 1.002)
                    # limit radius size
                    if seedRadius > 55:
                        seedRadius = \
                        55 * random.uniform(1.001, 1.002) 

                # spacing between orbits
                orbitRadius = \
                obitDistance * orbit * random.uniform(0.55, 0.85)

                # get random angle and x, y from seed location
                angle = random.uniform(0, 360)
                dotX = orbitRadius * math.cos(angle)
                dotY = orbitRadius * math.sin(angle)

                # add to seed location
                dotX = seedX + dotX
                dotY = seedY + dotY

                # don't plot dots where location is outside visible range, or when dot is less than 2px
                if dotX > - 100 and dotX < self.width + 100 and dotY + 100 > 0 and dotY < self.height + 100 and seedRadius > 2:

                    # create element
                    dot = self.dwg.add(self.dwg.circle(center = (dotX, dotY), \
                                               r=seedRadius * random.uniform(0.8, 1)))
                    # colour element
                    dotCol = self.__select_colour__(self.primary, self.accent1, self.accent2)
                    dot.fill(svgwrite.rgb(dotCol[0], dotCol[1], dotCol[2]), opacity = 0.75)

        return self.dwg

    def __stripes__(self):

        # define seed location
        left = 0
        right = self.width
        top = self.height
        bottom = 0

        # define stripe parameters
        thickness = random.uniform(350, 450)
        minThickness = 20
        minAngle = -15
        maxAngle = 15

        # select horizontal or vertical
        direction = random.random()

        if direction >= 0.5:
            # fewer stripes in vertical space
            numberStripes = 20
            print('Horizontal')
        else:
            # more stripes in horizontal space
            numberStripes = 50
            print('Vertical')

        for stripe in range(1, numberStripes):

            # set thickness of line
            thickness = thickness * random.uniform(0.5, 0.999)

            # restrict minimum thickness
            if thickness <= minThickness:
                thickness = minThickness*(1 + random.random())

            # set angle to skew line to
            angle = random.uniform(minAngle, maxAngle)

            # create either horizontal or vertical lines
            if direction >= 0.5:
                # vertical position of horizontal stripe stays within spread area
                vertPos = random.uniform(bottom + self.height / 6, top - self.height / 6)
                stripe = self.dwg.add(self.dwg.rect(insert = (left, vertPos), \
                                             size = (right, thickness)))
                # skew to predefined angle
                stripe.skewY(angle)
            else:
                # horizontal position of vertical stripe stays within spread area
                horiPos = random.uniform(left + self.width / 6, right - self.width / 6)
                stripe = self.dwg.add(self.dwg.rect(insert = (horiPos, bottom), \
                                            size = (thickness, top)))
                # skew to predefined angle
                stripe.skewX(angle)

            # colour element
            stripeCol = self.__select_colour__(self.primary, self.accent1, self.accent2)
            stripe.fill(svgwrite.rgb(stripeCol[0], stripeCol[1], stripeCol[2]), opacity = 0.75)

        return self.dwg

    def __tiles__ (self):

        # define sizing
        minSize = 100
        maxSize = 300
        initSize = random.uniform(minSize, maxSize)
        spacing = initSize / 8

        # number of tiles
        horNum = int(self.width / (initSize + spacing)) + 3
        vertNum = int(self.width / (initSize + spacing)) + 3

        # loop through horizontal tiles
        for horTile in range(1, horNum):

            # loop through vertical tiles
            for vertTile in range(1, vertNum):

                # set size
                size = initSize * random.uniform(0.95, 1.05)

                # set location
                vertLoc = ((initSize + spacing) * vertTile) - ((initSize + spacing) * random.uniform(0.95, 1.01))
                horLoc = ((initSize + spacing) * horTile) - ((initSize + spacing) * random.uniform(0.95, 1.01))

                # create element
                element = self.dwg.add(self.dwg.rect(insert = (horLoc, vertLoc), size = (size, size)))

                # rotate element
                element.rotate(random.uniform(-1, 1))

                # colour element
                elemCol = self.__select_colour__(self.primary, self.accent1, self.accent2)
                element.fill(svgwrite.rgb(elemCol[0], elemCol[1], elemCol[2]), opacity = 0.9)

        return self.dwg

# methods
def wallpaper_generator():
    syst = System()
    img = Image(syst.directory, [syst.width, syst.height])
    img.save()
    syst.set_wallpaper()
    # wait 15 minutes * 60 seconds
    time.sleep(15*60)
    # sart new thread
    t = threading.Thread(target = wallpaper_generator)
    t.start()

syst = System()
img = Image(syst.directory, [syst.width, syst.height])
img.save()
syst.set_wallpaper()